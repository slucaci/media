Media Manager package for laravel
===================

> **Add to "require" in composer.json:**
> "mediasun/media": "dev-master"

> **Add in composer.json: **
> "repositories":[
>  {
>    "type": "vcs",
>    "url":  "https://slucaci@bitbucket.org/slucaci/media.git"
>  }
>],

> **Add in "config/app.php" to providers array:**
> Mediasun\Media\MediaServiceProvider::class,


> **Run in command line:**
> php artisan vendor:publish 
> php artisan migrate

