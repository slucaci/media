<?php
Route::group(['middleware' => 'web'], function () {
  Route::get('media-manager',                   'Mediasun\Media\MediaController@index');
  Route::get('media-manager/get-all',           'Mediasun\Media\MediaController@getAll');
  Route::get('media-manager/view/{id}',         'Mediasun\Media\MediaController@view');
  Route::get('media-manager/reload-view/{id}',  'Mediasun\Media\MediaController@reloadView');
  Route::get('media-manager/delete/{id}',       'Mediasun\Media\MediaController@delete');
  Route::post('media-manager/upload',           'Mediasun\Media\MediaController@upload');
  Route::post('media-manager/resize/{id}',      'Mediasun\Media\MediaController@resize');
  Route::post('media-manager/crop/{id}',        'Mediasun\Media\MediaController@crop');
});
