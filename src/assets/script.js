$(document).ready(function(){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  Dropzone.options.dpzone = {
    init: function() {
      this.on("success", function(file, response) {
        $('#notifications').html('<div class="alert alert-' + response.type + '" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+response.message+'</div>');
        $('.loader').show();
        $.get( "/media-manager/get-all", function( data ) {
          $('#content').html(data);
          $('.loader').hide();
        });
      });
    }
  };
  $(document).on('click', '#upload-btn', function(ev) {
    ev.preventDefault();
    $('#upload-file').toggle()
  });

  $(document).on('click', '#view-edit-btn', function(ev) {
    ev.preventDefault();
    $('.media-edit-widget').toggle();
    $('#target').Jcrop({
  		onChange: showCoords,
  		onSelect: showCoords,
      trueSize: [$('#oWidth').val(),$('#oHeight').val()]
  	},function() {
        jcrop_api = this;
    });
  });

  $(document).on('blur', '#crop-width', function(ev) {
    ev.preventDefault();
    // set the selection area [left, top, width, height]
    var w = Math.round($('#crop-width').val());
    var h = Math.round($('#crop-height').val());

    jcrop_api.animateTo([0,0,w,h]);
  });

  $(document).on('blur', '#crop-height', function(ev) {
    ev.preventDefault();
    var w = Math.round($('#crop-width').val());
    var h = Math.round($('#crop-height').val());

    jcrop_api.animateTo([0,0,w,h]);
  });

  $(document).on('click', '#resize-image', function (ev) {
    ev.preventDefault();

    var postForm = {
      'saveas'     : $('input[name="saveas"]:checked').val(),
      'width'     : $('input[name="width"]').val(),
      'height'     : $('input[name="height"]').val(),
    };
    $('.loader').show();
    //console.log(postForm);
    request = $.ajax({
        url: $('#resize-url').val(),
        type: "post",
        data: postForm
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){

        $.get( "/media-manager/reload-view/" + response.id, function( data ) {
          window.history.pushState("Details", "Title", response.url);
          $('#content').html(data);
          $('.loader').hide();
        });
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
  });

  $(document).on('click', '#crop-image', function (ev) {
    ev.preventDefault();

    var postForm = {
      'saveas'         : $('input[name="saveas"]:checked').val(),
      'cropwidth'      : $('input[name="cropwidth"]').val(),
      'cropheight'     : $('input[name="cropheight"]').val(),
      'cropx'          : $('input[name="cropx"]').val(),
      'cropy'          : $('input[name="cropy"]').val(),
    };
    $('.loader').show();
    //console.log(postForm);
    request = $.ajax({
        url: $('#crop-url').val(),
        type: "post",
        data: postForm
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){

        $.get( "/media-manager/reload-view/" + response.id, function( data ) {
          window.history.pushState("Details", "Title", response.url);
          $('#content').html(data);
          $('.loader').hide();
        });
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
  });

  function showCoords(c) {
    var x = Math.round(c.x);
    var y = Math.round(c.y);
    var w = Math.round(c.w);
    var h = Math.round(c.h);
    var x2 = Math.round(c.x2);
    var y2 = Math.round(c.y2);

  	$('#crop-width').val(w);
  	$('#crop-height').val(h);
  	$('#cropx').val(x);
  	$('#cropx2').val(x2);
  	$('#cropy').val(y);
  	$('#cropy2').val(y2);
  };

});
