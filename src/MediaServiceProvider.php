<?php

namespace Mediasun\Media;

use Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider {

  public function boot (\Illuminate\Routing\Router $router) {

    $router->aliasMiddleware('media', '\Mediasun\Media\MediaMiddleware');
    
    $this->loadViewsFrom(__DIR__.'/views', 'media');

    // publish package views
    /*$this->publishes([
      __DIR__.'/views' => base_path('resources/views/vendor/media'),
    ]);*/

    // publish package config
    $this->publishes([
      __DIR__.'/config/media.php' => config_path('media.php'),
    ], 'media-config');

    // publish package libraries
    $this->publishes([
        __DIR__.'/lib' => public_path('medialib'),
    ]);

    $this->publishes([
        __DIR__.'/assets' => public_path('medialib/assets'),
    ], 'assets');

    // load package migrations
    $this->loadMigrationsFrom(__DIR__.'/migrations');
  }

  public function register () {
    include __DIR__.'/routes.php';
    $this->app->make('Mediasun\Media\MediaController');
  }

}
