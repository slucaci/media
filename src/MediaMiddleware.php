<?php
namespace Mediasun\Media;

use Closure;

class MediaMiddleware
{
    public function handle($request, Closure $next)
    {
      if (!\Auth::check()) {
        return redirect('/');
      }

      return $next($request);
    }
}
