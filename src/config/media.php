<?php
return [
  'media_path' => public_path() . '/media/',
  'media_real_path' => 'media/',
  'mimes' => 'jpeg,jpg,bmp,png,doc,docx,txt,zip,rar',
  'return_path' => '/',
];
