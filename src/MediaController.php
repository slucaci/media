<?php

namespace Mediasun\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;
use Storage, Validator;
use \Mediasun\Media\Media as Media;

class MediaController extends Controller {

  public function __construct () {
    $this->middleware('media');
  }

  public function index() {
    $files = Media::orderBy('id', 'desc')->paginate(28);
    return view("media::index", [
      'files' => $files,
    ]);
  }

  public function getAll () {
    $files = Media::orderBy('id', 'desc')->paginate(28);
    $files->setPath('/media-manager');

    return view("media::index-ajax", [
      'files' => $files
    ]);
  }

  public function view (Request $request, $id) {
    $file = Media::find($id);

    list($width, $height) = getimagesize($file->url);
    $bytes = filesize($file->url);
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    $decimals = 2;
    $fileSize = sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) ." ". @$size[$factor];

    return view("media::view", [
      'file' => $file,
      'imageInfo' => [
        'width'  => $width,
        'height' => $height,
        'size'   => $fileSize
      ]
    ]);
  }

  public function reloadView (Request $request, $id) {
    $file = Media::find($id);

    list($width, $height) = getimagesize($file->url);
    $bytes = filesize($file->url);
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    $decimals = 2;
    $fileSize = sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) ." ". @$size[$factor];

    return view("media::view-ajax", [
      'file' => $file,
      'imageInfo' => [
        'width'  => $width,
        'height' => $height,
        'size'   => $fileSize
      ]
    ]);
  }

  public function upload (Request $request) {
    $rules = array(
      'file' => 'required|mimes:' . config('media.mimes'),
    );

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {

      return response()->json([
        'status'  => 403,
        'type'    => 'danger',
        'message' => 'Forbidden ! File extension not allowed !',
      ]);
    } else {

      $file = $request->file('file');
      $fileName = time().$file->getClientOriginalName();
      $mime = $file->getMimeType();
      $file->move(config('media.media_path'). date('Y') . '/' . date('m'), $fileName);

      $media = new Media();
      $media->name = $file->getClientOriginalName();
      $media->url = config('media.media_real_path'). date('Y') . '/' . date('m') . '/' . $fileName;
      $media->type = $mime;
      $media->save();

      // create media thumb
      if (strpos($media->type , 'image') !== false) {
        $img = Image::make($media->url);
        $img->fit(400, 400, function ($constraint) {
          $constraint->upsize();
        });
        $img->save(config('media.media_real_path'). date('Y') . '/' . date('m') . '/thumb_' . $fileName);
        $media->thumb =  config('media.media_real_path'). date('Y') . '/' . date('m') . '/thumb_' . $fileName;
        $media->save();
      }
      return response()->json([
        'status'  => 200,
        'type'    => 'success',
        'message' => "The file " . $media->name . " is added to media library !",
      ]);
    }
  }

  public function resize (Request $request, $id) {
    // get media from database
    $media = Media::find($id);
    $returnId = $id;
    // get name of the image from full paath
    $explode = explode('/', $media->url);
    $name = last($explode);
    $year = $explode[1];
    $month = $explode[2];

    // edit image
    $img = Image::make($media->url);
    $img->fit($request->width, $request->height, function ($constraint) {
      $constraint->upsize();
    });

    if ($request->saveas == 2) {
      // override new image
      $img->save(config('media.media_real_path'). $year . '/' . $month . '/' . $name);
      // create thumb
      $img->fit(400, 400, function ($constraint) {
        $constraint->upsize();
      });
      $img->save(config('media.media_real_path'). $year . '/' . $month . '/thumb_' . $name);
      $media->thumb =  config('media.media_real_path'). $year . '/' . $month . '/thumb_' . $name;
      $media->save();

    } else {
      // add new image to database
      $name = time() . substr($name, 10);
      $new = new Media();
      $new->name = $media->name;
      $new->url = config('media.media_real_path'). date('Y') . '/' . date('m') . '/' . $name;
      $new->type = $media->type;
      $new->save();
      // save new image
      $img->save(config('media.media_real_path'). date('Y') . '/' . date('m') . '/' . $name);
      // generate thumb
      $img->fit(400, 400, function ($constraint) {
        $constraint->upsize();
      });
      // save thumb on disk
      $img->save(config('media.media_real_path'). date('Y') . '/' . date('m') . '/thumb_' . $name);
      // update thumb in database
      $new->thumb =  config('media.media_real_path'). date('Y') . '/' . date('m') . '/thumb_' . $name;
      $new->save();

      $returnId = $new->id;
    }

    return response()->json([
      'status'  => 200,
      'type'    => 'success',
      'message' => "The file " . $media->name . " is resized !",
      'id' => $returnId,
      'url' => '/media-manager/view/' . $returnId,
    ]);
  }

  public function crop (Request $request, $id) {
    // get media from database
    $media = Media::find($id);
    $returnId = $id;
    // get name of the image from full paath
    $explode = explode('/', $media->url);
    $name = last($explode);
    $year = $explode[1];
    $month = $explode[2];
    // edit image
    $img = Image::make($media->url);
    $img->crop(round($request->cropwidth), round($request->cropheight), round($request->cropx), round($request->cropy));

    if ($request->saveas == 2) {
      // override new image
      $img->save(config('media.media_real_path'). $year . '/' . $month . '/' . $name);
      // create thumb
      $img->fit(400, 400, function ($constraint) {
        $constraint->upsize();
      });
      $img->save(config('media.media_real_path'). $year . '/' . $month . '/thumb_' . $name);
      $media->thumb =  config('media.media_real_path'). $year . '/' . $month . '/thumb_' . $name;
      $media->save();

    } else {
      // add new image to database
      $name = time() . substr($name, 10);
      $new = new Media();
      $new->name = $media->name;
      $new->url = config('media.media_real_path'). date('Y') . '/' . date('m') . '/' . $name;
      $new->type = $media->type;
      $new->save();
      // save new image
      $img->save(config('media.media_real_path'). date('Y') . '/' . date('m') . '/' . $name);
      // generate thumb
      $img->fit(400, 400, function ($constraint) {
        $constraint->upsize();
      });
      // save thumb on disk
      $img->save(config('media.media_real_path'). date('Y') . '/' . date('m') . '/thumb_' . $name);
      // update thumb in database
      $new->thumb =  config('media.media_real_path'). date('Y') . '/' . date('m') . '/thumb_' . $name;
      $new->save();

      $returnId = $new->id;
    }

    return response()->json([
      'status'  => 200,
      'type'    => 'success',
      'message' => "The file " . $media->name . " is resized !",
      'id' => $returnId,
      'url' => '/media-manager/view/' . $returnId,
    ]);
  }

  public function delete (Request $request, $id) {
    $media = Media::find($id);
    \File::Delete(public_path() . '/' . $media->url);
    \File::Delete(public_path() . '/' . $media->thumb);
    $media->delete();

    return \Redirect::to('/media-manager');
  }

}
