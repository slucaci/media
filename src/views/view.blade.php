@extends('media::layouts.base')

@section('content')
  <div class="col-md-8 col-xs-12 no-padding padding-bottom-15">
    @if (strpos($file->type , 'image') !== false)
      <img src="{{URL::to($file->url)}}?{{rand(10000, 9999999)}}" alt="{{$file->name}}" class="img-responsive" class="view-details" id="target">
    @else
      <a href="{{URL::to('media-manager/view/' . $file->id)}}" class='center'>
        <span class="glyphicon glyphicon-file view-file-icon" aria-hidden="true"></span>
        <span class="view-file-title">{{$file->name}}</span>
      </a>
    @endif
  </div>
  <div class="col-md-4 col-xs-12">
    <div class="media-view-info">
      <div class="media-view-details">
        <ul>
          <li><strong>File type : </strong> {{$file->type}}</li>
          <li><strong>Uploaded at : </strong> {{$file->created_at->format('d-m-Y G:i:s')}}</li>
          <li><strong>File size : </strong> {{$imageInfo['size']}}</li>
          @if (strpos($file->type , 'image') !== false)
            <li><strong>File dimensions : </strong> {{$imageInfo['width']}} x {{$imageInfo['height']}}</li>
          @endif
          <li>
            <div class="form-group">
              <input type="text" class="form-control" id="media-url" value="{{URL::to($file->url)}}" readonly>
              <input type="hidden" id="resize-url" value="{{URL::to('media-manager/resize/' . $file->id)}}">
              <input type="hidden" id="crop-url" value="{{URL::to('media-manager/crop/' . $file->id)}}">
              <input type="hidden" id="media-id" value="{{$file->id}}">
              <input type="hidden" id="oWidth" value="{{$imageInfo['width']}}">
              <input type="hidden" id="oHeight" value="{{$imageInfo['height']}}">
            </div>
          </li>
        </ul>
      </div>
      @if (strpos($file->type , 'image') !== false)
        <div class="media-menu">
          <a class="btn btn-default" id="view-edit-btn"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit image</a>
          <a href="{{URL::to('media-manager/delete/' . $file->id)}}" class="btn btn-danger" id="view-delete-btn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete image</a>
        </div>
        <div class="media-edit-widget">
          <div class="radio">
            <label>
              <input type="radio" name="saveas" id="saveas-1" value="1" checked="checked">
              Save as new image
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="saveas" id="saveas-2" value="2">
              Override current image
            </label>
          </div>
          <div class="divider"></div>
          <p>Resize image: </p>
          <div class="form-group">
            <label for="width">Width</label>
            <input type="text" class="form-control" id="width" placeholder="Width" value="{{$imageInfo['width']}}" name="width">
          </div>
          <div class="form-group">
            <label for="cropheight">Height</label>
            <input type="text" class="form-control" id="cropheight" placeholder="Height" value="{{$imageInfo['height']}}" name="height">
          </div>
          <button id="resize-image" class="btn btn-primary">Resize</button>
          <div class="divider"></div>
          <p>Crop selected area</p>
          <div class="form-group">
            <label for="crop-width">Width</label>
            <input type="text" class="form-control" id="crop-width" placeholder="Width" name="cropwidth">
          </div>
          <div class="form-group">
            <label for="crop-height">Height</label>
            <input type="text" class="form-control" id="crop-height" placeholder="Height" value="" name="cropheight">
          </div>
          <input type="hidden" name="cropx" id="cropx">
          <input type="hidden" name="cropx2" id="cropx2">
          <input type="hidden" name="cropy" id="cropy">
          <input type="hidden" name="cropy1" id="cropy2">
          <button id="crop-image" class="btn btn-info">Crop</button>
        </div>
      @else
      <div class="media-menu">
        <a href="{{URL::to('media-manager/delete/' . $file->id)}}" class="btn btn-danger" id="view-delete-btn"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>
      </div>
      @endif
    </div>
  </div>
@stop
