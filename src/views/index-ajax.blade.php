@if($files->count())
  @foreach ($files as $file)
    @if (strpos($file->type , 'image') !== false)
      <div class="media-cover">
        <a href="{{URL::to('media-manager/view/' . $file->id)}}">
          <img src="{{URL::to($file->thumb)}}" alt="{{$file->name}}">
        </a>
      </div>
    @else
    <div class="media-cover">
      <a href="{{URL::to('media-manager/view/' . $file->id)}}">
        <span class="glyphicon glyphicon-file file-icon" aria-hidden="true"></span>
        <span class="file-title">{{$file->name}}</span>
      </a>
    </div>
    @endif
  @endforeach
@else
  <p>Nu exista fisiere de afisat.</p>
@endif
<div class="pagination">
  {{$files->links()}}
</div>
