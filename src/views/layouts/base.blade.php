<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tagsl *must* come first in the head; any other head content must come *after* these tags -->
    <title>Media Manager - V 1.0.0</title>

    <!-- Bootstrap -->
    <link href="{{URL::to('medialib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('medialib/dropzone/dropzone.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('medialib/jcrop2/css/jquery.Jcrop.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('medialib/assets/style.css')}}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container">
        <a href="{{URL::to(config('media.return_path'))}}" class="navbar-brand "><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
        <a href="{{URL::to('media-manager')}}" class="navbar-brand">Media Manager</a>
        <div class="pull-right">
          <a id="upload-btn" class="btn btn-default menu-btn">
            <span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Upload
          </a>
        </div>
      </div>
    </nav>
    <div class="container" id="notifications">

    </div>
    <div class="container" id="upload-file">
      <form action="/media-manager/upload" class="dropzone" id="dpzone">
        {{ csrf_field() }}
      </form>
    </div>
    <div class="container" id="content">
      @yield('content')
      <div class="loader">
        <div class="lds-css ng-scope"><div class="lds-spinner" style="100%;height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL::to('medialib/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::to('medialib/dropzone/dropzone.min.js')}}"></script>
    <script src="{{URL::to('medialib/jcrop2/js/jquery.Jcrop.min.js')}}"></script>
    <script src="{{URL::to('medialib/assets/script.js')}}"></script>
  </body>
</html>
